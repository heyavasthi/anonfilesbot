#    Copyright (C) 2021 - Avishkar Patil | @AvishkarPatil


import os
import sys
import time
import logging
import pyrogram
import aiohttp
import asyncio
import requests
import aiofiles
from random import randint
from progress import progress
from config import Config
from pyrogram.errors import UserNotParticipant, UserBannedInChannel
from pyrogram import Client, filters, idle
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup, CallbackQuery, InlineQuery, InputTextMessageContent


logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=logging.WARNING)

DOWNLOAD = "./"

# vars
APP_ID = Config.APP_ID
API_HASH = Config.API_HASH
BOT_TOKEN = Config.BOT_TOKEN

   
bot = Client(
    "AnonFilesBot",
    api_id=APP_ID,
    api_hash=API_HASH,
    bot_token=BOT_TOKEN)


START_TEXT = """
__Hᴇʟʟᴏ Dᴇᴀʀ I'ᴍ__ **ANONFILES BOT** 😎 \n\n__This bot can upload telegram files to Anonfiles(dot)com__\n\Maintained By__ :** @IshanAvasthi . Join @IshanUpdates**
"""
HELP_TEXT = """
**Anonfiles Bot Help**\n\n__Send me any telegram file to upload to anonfiles__\n\n__Developed By__ :** @IshanAvasthi**
"""
ABOUT_TEXT = """
- **Bot :** `AnonFilesBot`
- **Creator :** [Ishan Avasthi](https://telegram.me/IshanAvasthi)
- **Website :** [Click here](https://go.ishanavasthi.in/?utm_source=telegram)
- **Language :** [Python3](https://python.org)

__Maintained By__ :** @IshanAvasthi
"""

START_BUTTONS = InlineKeyboardMarkup(
        [[
        InlineKeyboardButton('Help', callback_data='help'),
        InlineKeyboardButton('About', callback_data='about'),
        InlineKeyboardButton('Close', callback_data='close')
        ]]
    )
HELP_BUTTONS = InlineKeyboardMarkup(
        [[
        InlineKeyboardButton('Home', callback_data='home'),
        InlineKeyboardButton('About', callback_data='about'),
        InlineKeyboardButton('Close', callback_data='close')
        ]]
    )
ABOUT_BUTTONS = InlineKeyboardMarkup(
        [[
        InlineKeyboardButton('Home', callback_data='home'),
        InlineKeyboardButton('Help', callback_data='help'),
        InlineKeyboardButton('Close', callback_data='close')
        ]]
    )


@bot.on_callback_query()
async def cb_data(bot, update):
    if update.data == "home":
        await update.message.edit_text(
            text=START_TEXT,
            disable_web_page_preview=True,
            reply_markup=START_BUTTONS
        )
    elif update.data == "help":
        await update.message.edit_text(
            text=HELP_TEXT,
            disable_web_page_preview=True,
            reply_markup=HELP_BUTTONS
        )
    elif update.data == "about":
        await update.message.edit_text(
            text=ABOUT_TEXT,
            disable_web_page_preview=True,
            reply_markup=ABOUT_BUTTONS
        )
    else:
        await update.message.delete()
        
        
@bot.on_message(filters.private & filters.command(["start"]))
async def start(bot, update):
    text = START_TEXT
    reply_markup = START_BUTTONS
    await update.reply_text(
        text=text,
        disable_web_page_preview=True,
        reply_markup=reply_markup
    )

      
@bot.on_message(filters.media & filters.private)
async def upload(client, message):
    if Config.UPDATES_CHANNEL is not None:
        try:
            user = await client.get_chat_member(Config.UPDATES_CHANNEL, message.chat.id)
            if user.status == "kicked":
                await client.send_message(
                    chat_id=message.chat.id,
                    text="**You are banned lol** [Developer](https://telegram.me/IshanAvasthi).",
                    parse_mode="markdown",
                    disable_web_page_preview=True
                )
                return
        except UserNotParticipant:
            await client.send_message(
                chat_id=message.chat.id,
                text="**Join Updates Channel to use me 🏃‍♂**",
                reply_markup=InlineKeyboardMarkup(
                    [
                        [
                            InlineKeyboardButton("Join Updates Channel", url=f"https://t.me/{Config.UPDATES_CHANNEL}")
                        ]
                    ]
                ),
                parse_mode="markdown"
            )
            return
        except Exception:
            await client.send_message(
                chat_id=message.chat.id,
                text="**Something went wrong! Contact my** [Developer](https://telegram.me/IshanAvasthi).",
                parse_mode="markdown",
                disable_web_page_preview=True)
            return
    m = await message.reply("**Downloading file to my server** 😈")
    now = time.time()
    sed = await bot.download_media(
                message, DOWNLOAD,
          progress=progress,
          progress_args=(
            "**Uploading started**\n**Time taken is a function of your file size.** \n\n**ETA:** ", 
            m,
            now
            )
        )
    try:
        files = {'file': open(sed, 'rb')}
        await m.edit("**Uploading to Anonfiles, please wait**")
        callapi = requests.post("https://api.anonfiles.com/upload", files=files)
        text = callapi.json()
        output = f"""
<u>**File Uploaded to Anonfiles.com**</u>

**📂 File Name:** {text['data']['file']['metadata']['name']}

**📦 File Size:** {text['data']['file']['metadata']['size']['readable']}

**📥 Download Link:** `{text['data']['file']['url']['full']}`

🔅__Developer__ :** @IshanAvasthi**"""
        btn = InlineKeyboardMarkup(
                                [[InlineKeyboardButton("Download File", url=f"{text['data']['file']['url']['full']}")]])
        await m.edit(output, reply_markup=btn)
        os.remove(sed)
    except Exception:
        await m.edit("__Process failed, maybe due to time out__")
        return
      
@bot.on_message(filters.regex(pattern="https://cdn-") & filters.private)
async def url(client, message):
    msg = await message.reply("__Checking URL__")
    lenk = message.text
    cap = "© @IshanAvasthi"
    thumb = "./thumb.jpg"
    try:
         await msg.edit("**Big Files take time**")
         filename = await download(lenk)
         await msg.edit("Uploading File To Telegram...")
         await message.reply_document(filename, caption=cap, thumb=thumb)
         await msg.delete()
         os.remove(filename)
    except Exception:
        await msg.edit("__Process failed, maybe due to time out!__")
        
async def download(url):
    ext = url.split(".")[-1]
    filename = str(randint(1000, 9999)) + "." + ext
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                f = await aiofiles.open(filename, mode='wb')
                await f.write(await resp.read())
                await f.close()
    return filename
        
        
bot.start()
print("AnonFilesBot Is Started!,  if Have Any Problems contact @IshanAvasthi")
idle()
